package wordle;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class WordleTests {
	@Test
	public void testPositionLettreNonComptabilisee() {
		boolean[] lettresComptabilisees = {false,true,false,false,false};
		assertEquals(2, Wordle.positionLettreNonComptabilisee("ESSAI", lettresComptabilisees, 'S'));
	}
	
	@Test
	public void testGetIndices() {
		assertEquals("--+  ", Wordle.getIndices("ESSAI", "TESTS"));
	}
	
	@Test
	public void testGetMessageErreur() {
		String[] motsAdmis = { "ESSAI", "TESTS", "MURET" };
		assertEquals("Veuillez entrer un mot de 5 lettres.", Wordle.getMessageErreur("MUR", 5, motsAdmis));
		assertEquals("Seules les lettres non accentuées sont admises", Wordle.getMessageErreur("MÈRES", 5, motsAdmis));
		assertEquals("Ce mot n'est pas dans la liste.", Wordle.getMessageErreur("SAINT", 5, motsAdmis));
	}
	
	@Test
	public void testDenombrerEssais() {
		String[][] parties = {{"SAINT", "ROULE", "NOUER", "MURER", "HUMER"}, 
				{"SAINT", "LOUPE", "GUISE", "LISSE", "TISSE", "VISSE","HISSE"}, 
				{"SAINT", "ROULE", "USINE", "TUILE", "FUITE"}
		};
		int[] tableauAttendu = {0,0,0,0,2,0,1};
		assertArrayEquals(tableauAttendu, Wordle.denombrerEssais(parties, 6));
	}
	
}
