package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TableauChainesTests {
	@Test
	public void testAjouterElement() {
		String[] tableauOrigine = {"SAINT", "ROULE", "NOUER"};
		String[] tableauAttendu = {"SAINT", "ROULE", "NOUER", "MURER"};
		assertArrayEquals(tableauAttendu, TableauChaines.ajouterElement(tableauOrigine, "MURER"));
		
		// Cas particulier où le tableau d'origine est vide
		String[] tableauVide = {};
		String[] nouveauTableauAttendu = {"MURER"};
		assertArrayEquals(nouveauTableauAttendu, TableauChaines.ajouterElement(tableauVide, "MURER"));
	}
	
	@Test
	public void testAjouterLigne() {
		String[][] tableauOrigine = {{"SAINT", "ROULE"},{"NOUER"}};
		String[] nouvelleLigne = {"MURER", "FUMER", "HUMER"};
		String[][] tableauAttendu = {{"SAINT", "ROULE"},{"NOUER"},{"MURER", "FUMER", "HUMER"}};
		assertArrayEquals(tableauAttendu, TableauChaines.ajouterLigne(tableauOrigine, nouvelleLigne));
	}
	
	@Test
	public void testContient() {
		String[] tableauOrigine = {"SAINT", "ROULE", "MURER"};
		assert(TableauChaines.contient(tableauOrigine, "MURER"));
		assertFalse(TableauChaines.contient(tableauOrigine, "PROUT"));
	}
}
