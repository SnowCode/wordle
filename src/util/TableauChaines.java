package util;

import java.util.Arrays;

public class TableauChaines {
	/**
	 * Crée un nouveau tableau d'une longueur de 1 plus long et y insère un nouvel élément
	 * @param t Tableau d'origine
	 * @param element Élément à ajouter dans le tableau
	 * @return Le nouveau tableau
	 */
	public static String[] ajouterElement(String[] t, String element) {
		String[] nouveauTableau = new String[t.length + 1];
		for (int i = 0; i < t.length; i++) {
			nouveauTableau[i] = t[i];
		}
		nouveauTableau[t.length] = element;
		return nouveauTableau;
	}
	
	/**
	 * Ajoute une nouvelle ligne à un tableau à deux dimensions
	 * @param t Le tableau à deux dimension
	 * @param ligne La ligne à ajouter (tableau à une dimension)
	 * @return Le nouveau tableau à deux dimensions
	 */
	public static String[][] ajouterLigne(String[][] t, String[] ligne) {
		String[][] nouveauTableau = new String[t.length + 1][];
		for (int i = 0; i < t.length; i++) {
			nouveauTableau[i] = t[i];
		}
		nouveauTableau[t.length] = ligne;
		return nouveauTableau;
	}
	
	/**
	 * Vérifie si un tableau contient une certaine chaine de caractère
	 * @param t Le tableau contenant les valeurs
	 * @param ch La chaine à trouver dans le tableau
	 * @return Renvoie true si la chaine est trouvée ou false si elle ne l'est pas
	 */
	public static boolean contient(String[] t, String ch) {
		for (String mot: t) {
			if (mot.equals(ch)) {
				return true;
			}
		}
		return false;
	}	
}
