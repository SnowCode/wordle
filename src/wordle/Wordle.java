package wordle;

import java.util.Arrays;

import io.Fichier;
import util.Aleatoire;
import util.TableauChaines;

public class Wordle {
	public static void main(String[] args) {
		int choix = 0;
		String[][] parties = {};
		while (choix != 3) {
			// Afficher le menu
			System.out.print("\nWORDLE\n1. Jouer une partie\n2. Afficher statistiques\n3. Quitter\nChoix ? ");
			choix = io.Console.lireInt();
			
			if (choix == 1) {
				// Liste des mots admis
				final String[] motAdmis = Fichier.lireToutesLesLignes("mots.txt");
				
				// Choix d'un motSolution parmis tout ça
				String motSolution = motAdmis[Aleatoire.aleatoire(0, motAdmis.length - 1)];
				System.out.println(motSolution);
				
				String[] partie = nouvellePartie(parties.length + 1, motSolution, 6, motAdmis);
				parties = TableauChaines.ajouterLigne(parties, partie);

			} else if (choix == 2) {
				System.out.println(Arrays.deepToString(parties));
				afficherStatistiques(parties, 6);
			}
		}
		
		System.out.println("\nFin du programme.");
	}
	
	
	/**
	 * Donne la position d'une lettre qui n'a pas encore été comptabilisée
	 * @param mot Le mot sur lequel il faut tester
	 * @param lettresComptabilisees La liste des positions déjà comptabilisées
	 * @param lettre Le caractère recherché
	 * @return La position (en entier) du caractère si il est trouvé, sinon retourne -1
	 */
	public static int positionLettreNonComptabilisee(String mot, boolean[] lettresComptabilisees, char lettre) {
		for (int i = 0; i < lettresComptabilisees.length; i++) {
			if (mot.charAt(i) == lettre && lettresComptabilisees[i] == false) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Donne les indices pour chaque lettre du mot joueur (+ si bonne lettre et bonne position, - si bonne lettre mais mauvaise position et espace si la lettre n'est pas présente dans le mot)
	 * @param motJoueur Mot tenté par le joueur
	 * @param motSolution Mot à trouver
	 * @return Les indices en String
	 */
	public static String getIndices(String motJoueur, String motSolution) {
		char[] indices = new char[motJoueur.length()];
		boolean[] lettresComptabilisees = new boolean[motSolution.length()];
		Arrays.fill(lettresComptabilisees, false);
		int longueur = lettresComptabilisees.length;
		for (int i = 0; i < longueur; i++) {
			if (motJoueur.charAt(i) == motSolution.charAt(i)) {
				indices[i] = '+';
				lettresComptabilisees[i] = true;
			}
		}
		
		for (int i = 0; i < longueur; i++) {
			if (indices[i] != '+') {
				int position = positionLettreNonComptabilisee(motSolution, lettresComptabilisees, motJoueur.charAt(i));
				if (position == -1) {
					indices[i] = ' ';
				} else {
					indices[i] = '-';
				}
			}
		}
		
		return new String(indices);
	}
	
	
	/**
	 * Retourne un message d'erreur si le motJoueur est invalide
	 * @param motJoueur Le mot tenté par le joueur
	 * @param nbLettres Le nombre de lettres obligatoire des mots
	 * @param motsAdmis La liste des mots autorisés
	 * @return Le message d'erreur
	 */
	public static String getMessageErreur(String motJoueur, int nbLettres, String[] motsAdmis) {
		if (motJoueur.length() != nbLettres) {
			return String.format("Veuillez entrer un mot de %d lettres.", nbLettres);
		} else if (!motJoueur.matches("[A-Z]*")) {
			return "Seules les lettres non accentuées sont admises";
		} else if (!TableauChaines.contient(motsAdmis, motJoueur)) {
			return "Ce mot n'est pas dans la liste.";
		} else {
			return null;
		}
	}
	
	public static String[] nouvellePartie(int numeroPartie, String motSolution, int maxEssais, String[] motsAdmis) {
		System.out.printf("\nPARTIE #%d%n", numeroPartie);
		String motJoueur = "";
		String indices = "";
		String message = null;
		int longueur = motSolution.length();
		String[] partie = {};
		String bonIndices = "+".repeat(longueur);
		while (!indices.equals(bonIndices) && partie.length < maxEssais) {
			do {
				System.out.printf("Mot %d ? ", partie.length + 1);
				motJoueur = io.Console.lireString();
				message = getMessageErreur(motJoueur, longueur, motsAdmis);
				if (message != null) {
					System.out.println(message);
				}
			} while (message != null);
			partie = TableauChaines.ajouterElement(partie, motJoueur);
			indices = getIndices(motJoueur, motSolution);
			System.out.printf("        %s%n", indices);
		}
		
		if (indices.equals(bonIndices)) {
			System.out.println("Bravo !");
		} else {
			partie = TableauChaines.ajouterElement(partie, motSolution);
			System.out.printf("Dommage ! Le mot à deviner était '%s'.%n", motSolution);
		}
		
		return partie;
	}
	
	/**
	 * Dénombre les essais dans un tableau d'entier
	 * @param parties Les parties en tableau à deux dimensions
	 * @param maxEssais Le maximum d'essais autorisés
	 * @return Le tableau d'entiers 
	 */
	public static int[] denombrerEssais(String[][] parties, int maxEssais) {
		int[] essais = new int[maxEssais + 1];
		Arrays.fill(essais, 0);
		for (String[] partie: parties) {
			essais[partie.length - 1]++;
		}
		
		return essais;
	} 
	
	public static void afficherStatistiques(String[][] parties, int maxEssais) {
		int[] essais = denombrerEssais(parties, maxEssais);
		int nombreParties = parties.length;
		int partiesPerdues = essais[maxEssais];
		int partiesGagnees = nombreParties - partiesPerdues;
		
		System.out.println("\nSTATISTIQUES");
		System.out.printf("Parties jouées = %d%n", parties.length);
		double pourcentage = ((double)partiesGagnees / nombreParties) * 100.0;
		System.out.printf("Victoires = %.1f%%%n", pourcentage);
		for (int i = 1; i <= maxEssais; i++) {
			if (essais[i - 1] == 0) {
				System.out.printf("%d = 0%n", i);
			} else {
				System.out.printf("%d = %s %d%n", i, "#".repeat(essais[i - 1]), essais[i - 1]);
			}
		}
		
		if (essais[maxEssais] == 0) {
			System.out.println("X = 0");
		} else {
			System.out.printf("X = %s %d%n", "#".repeat(essais[maxEssais]), essais[maxEssais]);

		}
	}

}
